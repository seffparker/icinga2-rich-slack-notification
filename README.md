# icinga2-rich-slack-notification

This is an improved version of https://github.com/nisabek/icinga2-slack-notifications with rich notification format, more details, and extensive configuration.

# Preview
![Sample Notification Preview](https://bitbucket.org/seffparker/icinga2-rich-slack-notification/raw/591a33be84591f167805f80cfa78df6f82f23087/preview.png "Sample Notification Preview")

# Additional Features
1. Rich Slack notification using JSON attachments.
2. Colored Notification.
3. Shows alert state-duration in human readable format.
4. Can send notifications to multiple Slack teams.
5. Customizable footer text